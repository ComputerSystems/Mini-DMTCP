# Mini-DMTCP

### Problem Description:

We have to write a program that can create a checkpoint image of a running process and a restart program that can restart the process from the checkpointed image file.
This should work for any process that is a single threaded application with no shared memory.

For example, Lets consider a process that prints from 1 to 100 inside a while loop with 2sec sleep between each iteration. 
We should be able to create checkpoint on that process at any time and the restart program should be able to restart the process from where it was checkpointed.
i.e. The process should print from where it was checkpointed.

For full description,
http://www.ccs.neu.edu/home/kapil/courses/cs5600f17/hw1.html


### Design:

#### Checkpoint creation 

1. The checkpoint creator program(ckpt.c) handles SIGUSR2 and creates checkpoint of the current process.
2. To be able to attach this with any program, We create a shared library.(libckpt.so). Any program can run by preloading(using LD_PRELOAD) the shared library.
3. Signal handler is inside the library. We don't expect processes that preload the library to call the handler in its main(). So, we use contructor.
4. To create checkpoint image, we have to copy the data in the memory region (/proc/self/maps) and context info using getcontext() function.
5. We define a struct (MemoryRegion) to hold the important info of the memory region. i.e. start addr, end addr and permission (rwxp) data. 
6. For each memory region, we save the struct followed by the data present in those regions. And finally we save the context info.
7. This data is stored in the following format in the checkpoint image (myckpt): MR1, DATA1, MR2, DATA2 ... MRn, DATAn, EMPTY_MR, context
8. This EMPTY_MR is the dummy entry to denote the start of context info.

#### Restart

1. We make the program load statically at some predefined address(Text: 0x5000000, DataSeg: 0x5100000, bss: 0x5200000) to avoid memory clash with the checkpointed process.
2. We move the entire stack to some unused position (ex. 0x5300000) So that the checkpointed process' stack won't overlap with it.
3. After moving the stack to new address, we have to use inline assembly code to set the stack pointer to appropriate location.
4. Now, from the myckpt file, we read the memory region range, mmap required range in memroy and move the data over there.
5. Finally, we get the context info from the file and restart the process using setcontext()

### Setup and Configuration

ckpt.c is the checkpoint creator file. myrestart.c is the restart program. hello.c is our test program. myckpt is the checkpointed image binary file.
1. make ckpt - Creates shared library libckpt.so
2. make hello - Creates hello binary
3. make test - Runs hello program with libckpt.so preloaded.
4. send_sigusr2.sh - Can be used to send SIGUSR2 to hello program.(To create checkpoint)
5. make res - Restarts the program from where it was checkpointed.
6. make clean-all - Cleans all binary files.

### Challenges/Learnings

#### Stack smashing

I had the argument mr_fd in save_context() function in ckpt.c, Checkpoint was created with the fd open inside that function. During setcontext it took a random value since we didn't copy open fds. 
This caused stack smashing. I moved getcontext outside and the issue was resolved.

#### Differentiating ckpt and myrestart process after setcontext

When we do setcontext, the process restarts exactly after the getcontext() call. We need to have something that tells which is the ckpt process and which is myrestart process. So I maintained pid as 
global variable and did a comparison with getpid() to decide which process to be continued.


