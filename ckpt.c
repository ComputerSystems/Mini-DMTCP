/*
 * Sriramprabhu Sankaraguru
 * 001648794
 * */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <ucontext.h>
#include <string.h>
#include <stdbool.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>

/*
 * Libckpt.so : Handles SIGUSR2 signal and creates a checkpoint image.
 * */

//Enable debug flag for debugging.
//#define DEBUG 1

#define BUFF_SIZE 1024
#define LINE_LEN 1000
#define FPATH_LEN 50

#define PROC_MAPS_FILE "/proc/self/maps"
#define CKPT_FILE_NAME "./myckpt"

#define ADDR_STR_LEN 20
#define PERM_STR_LEN 5
#define CURR_STR_LEN 100

//MemoryRegion Structure.
typedef struct MemoryRegion{
    void *startAddr;
    void *endAddr;
    int isReadable;
    int isWritable;
    int isExecutable;
    int isPrivate;
}MemoryRegion;

static pid_t pid;
static ucontext_t context;

/*
 * Loads data into the MemoryRegion struct Instance from the given args
 * Args: 
 * start_addr - Starting address string from proc file.
 * end_addr - end address string
 * perm - permission bit string
 * Returns:
 *  None
 * */
void load_MR_instance(MemoryRegion *mr, char* start_addr, char *end_addr, 
        char* perm){
    long SA, EA;
    //convert string to long and make them as pointers
    SA = strtol(start_addr, NULL, 16);
    EA = strtol(end_addr, NULL, 16);
    if(SA == 0L || EA == 0L){
#ifdef DEGUG
        printf("Address conversion error. Continue as of now");
#endif
        return;
    }
    void *startAddr = (long *)SA;
    void *endAddr = (long *)EA;
    mr->startAddr = startAddr;
    mr->endAddr = endAddr;
    //perm string will be of 'rwxp' format. 
    if(perm[0] != '-'){
        mr->isReadable = 1;
    }
    if(perm[1] != '-'){
        mr->isWritable = 1;
    }
    if(perm[2] != '-'){
        mr->isExecutable = 1;
    }
    if(perm[3] == 'p'){
        mr->isPrivate = 1;
    }
}

/*
 * Copy and then reset the curr_str to the corresponding char array
 * Which could be either one of Start Addr, End Addr, perm
 * Args:
 *  curr_str - Char array that holds the char buffered so far
 *  dest - Corresponding char array where the buffered data to be stored.
 *  end - End index of curr_str array. Reset it after we do the copy.
 * Returns:
 *  None
 * */
void copy_curr_str(char *curr_str, char *dest, int *end){
    curr_str[*end] = '\0';
    strcpy(dest, curr_str);
    strcpy(curr_str, "");
    *end = 0;
}

/*
 * Given the memory region line from proc file, populate MemoryRegion instance
 * Args:
 * line - memory region line as char array.
 * mr - pointer to the MemoryRegion struct which has to be populated
 * Returns:
 *  None
 * Extract first two fields. i.e. Memory Regions, Permissions
 * Extract start and end address, permissions and populate the struct
 * */
void populate_MR_instance(char *line, MemoryRegion *mr){

    char start_addr[ADDR_STR_LEN], end_addr[ADDR_STR_LEN], perm[PERM_STR_LEN],
    curr_str[CURR_STR_LEN];  
    int i,j;
    bool foundSA = false, foundEA = false;

    if(!line){
        return;
    }
    //Keep building curr_str until we Find SA. If found, make a copy of curr_str
    //and reset it. Proceed with EA, and stop once we have found permission str
    for( i = 0, j = 0; line[i] != '\0'; i++ ){
        if(line[i] == ' '){
            if(foundSA && !foundEA){
                copy_curr_str(curr_str, end_addr, &j);
                foundEA = true;
            }
            else if(foundSA && foundEA){
                copy_curr_str(curr_str, perm, &j);
                break;
            }
            else{
                break;
            }      
        }
        else if(line[i] == '-'){
            if(!foundSA){
                copy_curr_str(curr_str, start_addr, &j);
                foundSA = true;
            }
            else{
                curr_str[j++] = '-';
            }
        }
        else{
            curr_str[j++] = line[i];
        }
    }
    load_MR_instance(mr, start_addr, end_addr, perm);
}

/*
 * Write the section header to ckpt image file.
 * Args:
 *  mr_fd - ckpt image file descriptor
 *  mr - MemoryRegion structure.
 * Returns:
 *  None
 * */
void write_section_header(int mr_fd, MemoryRegion mr){
    ssize_t len = write(mr_fd, &mr, sizeof(mr));
    if(len <= 0){
        printf("Section Header hasn't Written to the file. ERRNO: %d\n", errno);
        exit(-1);	
    }
}
/*
 * Write the data in the memory region to ckpt image file.
 * Args:
 *  mr_fd - file descriptor to ckpt image file.
 *  mr - MemoryRegion Structure
 * Returns:
 *  None
 * */
void write_MR_data(int mr_fd, MemoryRegion mr){
    size_t count = mr.endAddr - mr.startAddr;
#ifdef DEBUG
    printf("Writing %ld bytes\n", count);	
#endif
    ssize_t len = write(mr_fd, mr.startAddr, count);
    if(len < 0){
        printf("Data in MR wasn't Written to the file.ERRNO: %d-%p\n", errno,mr.startAddr);
        exit(-1);	
    }
#ifdef DEBUG
    printf("Completed Writing of %ld bytes\n", len);
#endif
}

/*
 * Check the the region has to be copied to ckpt image.
 * Args:
 *  line - A single memory region line from proc file.
 * Returns:
 *  True if needed false otherwise.
 * */
bool is_necessary_region(char *line){	
    if(strstr(line, "[vsyscall]") != NULL || line[0] == 'f'){
        return false;
    }
    else
        return true;
}

/*
 * check if the regions should be written to file and write it if needed
 * Args:
 *  line - A single memory region line from proc file
 *  mr_fd - file descriptor to the ckpt image file.
 * Returns:
 *  None
 * */
void check_and_write_region(char *line, int mr_fd){
    if(!is_necessary_region(line))
        return;	
#ifdef DEBUG
    printf("Necessary Region %s\n", line);
#endif
    MemoryRegion mr = {NULL, NULL, 0, 0, 0};
    populate_MR_instance(line, &mr);	
#ifdef DEBUG
    printf("%p-%p %d %d %d\n", mr.startAddr, mr.endAddr, mr.isReadable,
            mr.isWritable, mr.isExecutable); 
#endif
    if(mr.isPrivate && mr.isReadable){
        write_section_header(mr_fd, mr);		
        write_MR_data(mr_fd, mr);
    }

}

/*
 * Create a binary file and For Each memory Region, Save 
 * 1. section header
 * 2. Followed by Data in the memory region.
 * Args:
 *  mr_fd - FD to the ckpt image file.
 * Returns:
 *  None
 * */
void save_memory_regions(int mr_fd){

    char file_path[FPATH_LEN] = PROC_MAPS_FILE;  
    int proc_fd = open(file_path, O_RDONLY);  
    if(proc_fd < 0)
    {
        printf("Couldn't Open The File %s\n", file_path);
        return;
    }
#ifdef DEBUG
    printf("Reading File %s\n", file_path);  
#endif
    char line[LINE_LEN];
    char buff[BUFF_SIZE];  
    ssize_t len;
    int i, j = 0;
    while((len = read(proc_fd, &buff, BUFF_SIZE)) > 0){
        buff[BUFF_SIZE-1] = '\0';
        for(i = 0; i < len; i++){
            //If we have found a full line, process it.			
            if(buff[i] == '\n'){
                line[j] = '\0';
                j = 0;
                check_and_write_region(line, mr_fd);
            }
            //Keep building line otherwise.
            else
                line[j++] = buff[i];
        }
    }  
    if(close(proc_fd) < 0){
        printf("Couldn't close the file. %d\n", errno);
        exit(-1);
    }
    return;
}

/*
 * save the context data to file.
 * Args:
 *  mr_fd - FD to the ckpt image file
 * Returns:
 *  None
 * */
void save_context(int mr_fd){

    //write an empty MR just to denote end of memory regions
    MemoryRegion mr = {NULL, NULL, 0,0,0,0};
    ssize_t len = write(mr_fd, &mr, sizeof(MemoryRegion));
    if(len <= 0){
        printf("Context data wasn't Written to the file. ERRNO: %d\n", errno);
        exit(-1);	
    }
    //Now write context info.
    len = write(mr_fd, &context, sizeof(ucontext_t));
    if(len <= 0){
        printf("Context data wasn't Written to the file. ERRNO: %d\n", errno);
        exit(-1);	
    }

}

/*
 * SIGUSR2 Handler.
 * Save the memory regions and context info to the ckpt image file.
 * */
void sigusr_handler(int signum){

    printf("Sig USR2 received...Creating Checkpoint image...\n");

    int mr_fd = open(CKPT_FILE_NAME, O_CREAT|O_WRONLY|O_TRUNC, 0666);
    if(mr_fd < 0){
        printf("File create Failed. ERRNO:%d\n",errno);
        exit(-1); 
    }

    save_memory_regions(mr_fd);	
    //when we call setcontext, it starts executing exactly after the getcontext
    //call. So we need a differentiation here. So, we Use PID. 
    pid = getpid();

    if(getcontext(&context) < 0){
        printf("Get Context Failed. %d\n", errno);
        exit(-1);	
    }
    //This path is taken if its hello process. i.e. saving context
    if(pid == getpid()){
#ifdef DEBUG
        printf("After Get context \n");
#endif
        save_context(mr_fd);			
        if( close(mr_fd) < 0 ){
            printf("Error on FD Close. ERRNO: %d\n", errno);
        }
        printf("Process checkpointed successfully.\n");	
    }
    //This is from myrestart process
    else{
        printf("Set context completed... Restarting Process...\n");	
    }

}

/*
 * Global constructor. Just to register for the signal handler.
 * */
__attribute__ ((constructor))
    void ckptconstructor(){
        signal(SIGUSR2, sigusr_handler);
    }

