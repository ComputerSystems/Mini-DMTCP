#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <ucontext.h>
#include <string.h>
#include <stdbool.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/mman.h>

/*
 * Restarts the process from checkpoint image
 * */

//Enable debug flag for debugging.
//#define DEBUG 1

#define BUFF_SIZE 1024
#define LINE_LEN 200
#define FPATH_LEN 50

#define PROC_MAPS_FILE "/proc/self/maps"

#define ADDR_STR_LEN 20
#define PERM_STR_LEN 5
#define CURR_STR_LEN 100

//Stack will be loaded at 0x5300000
#define STACK_START_ADDR 87031808
#define STACK_LENGTH 150000

/*
 * Following Data should be available even after we unmap the stack.
 * So declare them as global so they will reside in Data segment.
 * */
//Check point image name
char ckpt_image[1000];
//Context info
ucontext_t context;
//Old stack's start and end addresses
long SA, EA;

//MemoryRegion Structure.
typedef struct MemoryRegion{
    void *startAddr;
    void *endAddr;
    int isReadable;
    int isWritable;
    int isExecutable;
    int isPrivate;
}MemoryRegion;

/*
 * Retrieves context from ckpt image and calls setcontext.
 * Args:
 *  fd - FD for Ckpt image file
 * Returns:
 *  None
 * */
void restore_context(int fd){
    ssize_t len = read(fd, &context, sizeof(ucontext_t));
    if(len < 0){
        printf("Read Error:%d\n",errno);
        exit(-1);
    }
#ifdef DEBUG
    printf("%ld is the size of context\n",len);
#endif	
    int ret  = setcontext(&context);
    if(ret < 0){
        printf("Error in set context : %d\n", errno);
        exit(-1);
    }
}

/*
 * Given MemoryRegion from ckpt image, maps the corresponding region
 * to the restart process using mmap.
 * Args:
 *  fd - FD for ckpt image file
 *  mr - MemoryRegion
 * Returns:
 *  None
 * */
void map_MR_data(int fd, MemoryRegion mr){
    size_t count = mr.endAddr - mr.startAddr;
#ifdef DEBUG
    printf("Copying %ld bytes\n", count);	
    printf("%p-%p %d %d %d\n", mr.startAddr, mr.endAddr, mr.isReadable,
            mr.isWritable, mr.isExecutable); 
#endif

    void *region_start = mr.startAddr;
    //We have to allocate memory from the starting address.
    void *mem_region = mmap(mr.startAddr, count, PROT_READ | PROT_WRITE
            | PROT_EXEC, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0); 
    if(mem_region == MAP_FAILED){
        printf("Mmap failed %d\n",errno);	
        exit(-1);
    }
#ifdef DEBUG
    printf("%p successful\n", mem_region);
#endif
    ssize_t len = read(fd, mem_region, count);
    if(len < 0){
        printf("Data in MR wasn't read from the file.ERRNO: %d\n", errno);
        exit(-1);  
    }	
#ifdef DEBUG
    printf("Done copying %ld bytes\n", len);
#endif
    return;
}

/*
 * Retrieves MemoryRegions from ckpt image file and creates the corresponding
 * regions in the restart process.
 * Args:
 *  fd - FD for ckpt image file.
 * Returns:
 *  None
 * */
void restore_memory_region(int fd){	
    while(true){
        MemoryRegion mr;
        ssize_t len = read(fd, &mr, sizeof(MemoryRegion));
        if(len == 0){
            break;
        } 
        if(len < 0){
            printf("Read Error:%d\n",errno);
            exit(-1);
        }
        //This is the empty region added to denote end of memory regions
        if(mr.startAddr == NULL)
            break;
        map_MR_data(fd, mr);  
    }	
}

/*
 * Restarts the process by restoring Memory regions and context info.
 * Args:
 *  None
 * Returns:
 *  None
 * */
void restore_process(){
    void *old_stack = (long *)SA;	
#ifdef DEBUG
    printf("old stack pointer %p\n", old_stack);
#endif
    //Stack of the current process has been moved to a new place.
    //We have to unmap that region to avoid clashes with process that is going
    //to be restored.
    if( munmap(old_stack, (	EA - 	SA)) < 0)
    {
        printf("munmap failed. errno: %d\n",errno);	
        exit(-1);
    }  
#ifdef DEBUG
    printf("munmap successful\n");
#endif
    int fd = open(ckpt_image , O_RDONLY);
    if(fd < 0){
        printf("File open error%d\n",errno);
        exit(-1);
    }

    //restore memory region
    restore_memory_region(fd);
    //restore context
    restore_context(fd);	
    if(close(fd) < 0){
        printf("File close failed. %d\n",errno);	
    }
}

/*
 * Moves the stack region to a new place since this region will be used by
 * process which is going to be restarted.
 * Args:
 *  SA - Starting Address value
 *  EA - End address value.
 *  stack_region - pointer to the Region where the stack should be moved
 * Returns:
 *  None
 * */
void move_stack_region(void *stack_region){
#ifdef DEBUG
    printf("Moving stack Region\n");
#endif
    //Copy the data
    memcpy(stack_region, (long *) SA, EA-SA);
    stack_region += (EA-SA);
#ifdef DEBUG
    printf("Assembly call\n");
#endif
    //assembly code to move stack pointer.
    asm volatile("mov %0,%%esp" : : "g" (stack_region) : "memory");
    //After this, no function should return since they will refer the old stack
    //and result in SIGSEGV
    restore_process();	
}

/*
 * Copy and then reset the curr_str to the corresponding char array
 * Which could be either one of Start Addr, End Addr, perm
 * Args:
 *  curr_str - Char array that holds the char buffered so far
 *  dest - Corresponding char array where the buffered data to be stored.
 *  end - End index of curr_str array. Reset it after we do the copy.
 * Returns:
 *  None
 * */
void copy_curr_str(char *curr_str, char *dest, int *end){
    curr_str[*end] = '\0';
    strcpy(dest, curr_str);
    strcpy(curr_str, "");
    *end = 0;
}

/*
 * check if its stack region and shifts the region.
 * Args:
 *  line - A single line entry from proc file.
 *  stack_region - Pointer to newly allocated stack.
 * Returns:
 *  1 - if successful
 *  0 - otherwise
 * */
int check_and_copy_region(char *line, void *stack_region){

    if(strstr(line, "[stack]") == NULL)
        return 0;

    char start_addr[ADDR_STR_LEN], end_addr[ADDR_STR_LEN],
    curr_str[CURR_STR_LEN];
    int i,j;
    bool foundSA = false, foundEA = false;
    for( i = 0, j = 0; line[i] != '\0'; i++ ){
        if(line[i] == ' '){
            if(foundSA && !foundEA){
                copy_curr_str(curr_str, end_addr, &j);
                break;
            }      
            else{
                break;
            }      
        }
        else if(line[i] == '-'){
            if(!foundSA){
                copy_curr_str(curr_str, start_addr, &j);
                foundSA = true;
            }
            else{
                break;
            }
        }
        else{
            curr_str[j++] = line[i];
        }
    }
    SA = strtol(start_addr, NULL, 16);
    EA = strtol(end_addr, NULL, 16);
#ifdef DEBUG
    printf("%ld %ld\n", SA, EA);
#endif
    return 1;
}

/*
 * Copy the stack region to the newly allocated space 
 * Args:
 *  stack_region - pointer to the newly allocated stack region.
 * Returns:
 *  None
 * */
void copy_initial_stack(void *stack_region){
    //Inital stack region will be present in /proc/self/maps
    char file_path[FPATH_LEN] = PROC_MAPS_FILE;  
    int proc_fd = open(file_path, O_RDONLY);  
    if(proc_fd < 0)
    {
        printf("Couldn't Open The File %s\n", file_path);
        return;
    }
#ifdef DEBUG
    printf("Reading File %s\n", file_path);  
#endif
    char line[LINE_LEN];
    memset(line, 0, LINE_LEN); 
    char buff[BUFF_SIZE];  
    memset(buff, 0, BUFF_SIZE); 
    ssize_t len;
    while((len = read(proc_fd, &buff, BUFF_SIZE)) > 0){
        int i, j;		
        for(i = 0, j = 0; i < len; i++){
            //If we have found a full line, process it.			
            if(buff[i] == '\n'){
                line[j] = '\0';
                //Check if this is stack region else continue
                if(check_and_copy_region(line, stack_region) != 0)
                    break;
                j = 0;
                memset(line, 0, LINE_LEN);
            }
            //Keep building line otherwise.
            else
                line[j++] = buff[i];
        }
        memset(buff, 0, BUFF_SIZE);
    }  
    if(close(proc_fd) < 0){
        printf("File close failed. %d\n",errno);	
    }

    return;
}

/*
 * Shifts the stack of current process to a new location. (ox5300000) Which wont
 * clash with the process to be restarted. And then Unmaps the old stack.
 * Args:
 *  None
 * Returns:
 *  None
 * */
void shift_stack(){
    //Move to 0x5300000 which is least likely to be used by any process.
    long start_addr = STACK_START_ADDR;
    void *stack_start = (long *)start_addr;
    size_t stack_size = STACK_LENGTH;
    //Allocate memory for the region.
    void *stack_region = mmap(stack_start, stack_size, PROT_READ | PROT_WRITE,
            MAP_PRIVATE | MAP_ANONYMOUS, -1, 0); 
    if(stack_region == MAP_FAILED){
        printf("Mmap failed %d\n",errno);	
        exit(-1);
    }
#ifdef DEBUG
    printf("%p\n", stack_region);
#endif
    //Now copy the data to the new region
    copy_initial_stack(stack_region);
    move_stack_region(stack_region);	
}

int main(int argc, char **argv){
    if(argc != 2){
        printf("Usage: ./myrestart <CheckPointImage Name>\n");
        return -1;
    }
    strcpy(ckpt_image, argv[1]);
#ifdef DEBUG
    printf("Using %s as check point image\n", ckpt_image);
#endif
    //shift the stack to different memory region.
    shift_stack();
    return 0;
}
