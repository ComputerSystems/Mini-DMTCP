#include <stdio.h>
#include <unistd.h>
/*
 * Simple file to count from 1 inside infinte loop
 * */
int i = 1;
int main(){
    while(1){  
        printf("%d\n", i++);
        sleep(2);
    }
    return 0;
}
