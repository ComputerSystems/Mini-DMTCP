#Mini-DMTCP Makefile

all:	check

default: check
	
clean-ckpt:
	rm -rf myckpt

clean: clean-ckpt
	rm -rf myrestart core ckpt libckpt.so

clean-all: clean
	rm -rf hello

ckpt: 
	gcc -g -shared -o libckpt.so -fPIC ckpt.c

hello: hello.c
	gcc -g  -Wall -Werror -fpic hello.c -o hello

restart: myrestart.c
	gcc -g -static -Wl,-Ttext-segment=5000000 -Wl,-Tdata=5100000 -Wl,-Tbss=5200000 -o myrestart myrestart.c

res: 	restart
	./myrestart myckpt

gdb:
	gdb --args ./myrestart myckpt

check:	clean-ckpt libckpt.so hello restart
	(sleep 4 && kill -12 `pgrep -n hello` && sleep 2 && pkill -9 hello) & 
	LD_PRELOAD=`pwd`/libckpt.so ./hello
	(sleep 2 &&  pkill -9 myrestart) &
	make res

test:
	LD_PRELOAD=./libckpt.so ./hello 

dist:
	dir=`basename $$PWD`; cd ..; tar cvf $$dir.tar ./$$dir; gzip $$dir.tar
